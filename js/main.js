document.getElementById("hello_text").textContent = "はじめてのJavaScript";

var count = 0;
var cells;
var fallingblockclass;
var fallingblockpattern;
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]

        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
    
};


loadTable();
setInterval(function () {
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
    for (var row = 0; row < 2; row++){
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum != null && isFalling === false) {
                alert("game over");
              }
        }
    }
    if (hasFallingBlock()) {
        fallBlocks();
    } else {
        deleteRow();
        generateBlock();
    }
    
}, 1000);

function loadTable() {
    var td_array = document.getElementsByTagName("td");
    cells = [];
    var index = 0;
    for (var row = 0; row < 20; row++) {
        cells[row] = [];
        for (var col = 0; col < 10; col++) {
        cells[row][col] = td_array[index];
        index++;
        }
    }
}
function fallBlocks() {
    for (var col = 0; col < 10; col++){
        if (cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            return;
        }
    }

    for (var row = 18; row >=　0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
                isFalling = false;
                return;
                }
            }
        }
    }
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var fallingBlockNum = 0;

function generateBlock() {
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;

    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++){
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

function deleteRow() {
    //削除するべきかを確認
    for (var row = 19; row >= 0; row--){
        var delete_trigger = true;
        for (var col = 0; col < 10; col++){
            if (cells[row][col].className === ""){
                delete_trigger = false;
            }
        }
        if (delete_trigger === true){
            for (var col = 0; col < 10; col++){
                cells[row][col].className = "";
                cells[row][col].blockNum = null;

            }
            for (nextrow = row; nextrow >= 1; nextrow--){
                for (var col = 0; col < 10; col++){
                    cells[nextrow][col].blockNum = cells[nextrow - 1][col].blockNum;
                    cells[nextrow][col].className = cells[nextrow - 1][col].className;
                    cells[nextrow - 1][col].blockNum = null;
                    cells[nextrow - 1][col].className = "";
                }
            }
            row += 1;
        }
    }
}

var isFalling = false;
function hasFallingBlock() {
    return isFalling;
}

document.addEventListener("keydown", onKeyDown);

function onKeyDown(event) {
    if (event.keyCode === 37) {
        moveLeft();
    } else if (event.keyCode === 39) {
        moveRight();
    }
    else if (event.keyCode === 40) {
        moveDown();
    }
    else if (event.keyCode === 65) {
        spinRight();
    }
}

function　moveRight() {
    for (var row = 0; row < 20; row++) {
        if (cells[row][9].blockNum === fallingBlockNum) {
            return;
        }
    }
    //今落ちているブロックの右端側に他のブロックがあったらうごかないようにする。
    for (var row = 19; row >= 0; row--) {
        for (var col = 8; col >= 0; col--) {
            if (cells[row][col].blockNum == fallingBlockNum　&& cells[row][col + 1].blockNum != fallingBlockNum && cells[row][col + 1].blockNum != null) {
                return;
             }
        }
    }

    for (var row = 19; row >= 0; row--) {
        for (var col = 8; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
function　moveLeft() {
    for (var row = 0; row < 20; row++) {
        if (cells[row][0].blockNum === fallingBlockNum) {
            return;
        }
    }
    //今落ちているブロックの左端側に他のブロックがあったらうごかないようにする。
    for (var row = 19; row >= 0; row--) {
        for (var col = 9; col >= 1; col--) {
            if (cells[row][col].blockNum == fallingBlockNum　&& cells[row][col - 1].blockNum != fallingBlockNum && cells[row][col - 1].blockNum != null) {
                return;
             }
        }
    }
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
function　moveDown() {
    for (var col = 0; col < 10; col++){
        if (cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            return;
        }
    }

    for (var row = 18; row >=　0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
                isFalling = false;
                return;
                }
            }
        }
    }
    for (var row = 19; row >= 0; row--) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

/*function trans(ro , co) {
    var edge;
    if (array.length >= array[0].length){
        edge = array.length;
    }
    else {
        edge = array[0].length;
    }
    var(var transcount = edge ; )
}
*/
function spinRight() {
    var elementcount = 0;
    var varfirstrow = [];
    var varfirstcol = [];
    console.log(elementcount);
    aloop:
    for (var col = 9; col >= 0; col--) {
        for (var row = 19; row >= 0; row--) {
            if (cells[row][col].blockNum == fallingBlockNum) {
                varfirstcol[elementcount] = col;
                varfirstrow[elementcount] = row;
                elementcount += 1;
                console.log(elementcount);
                /*if(elementcount == 4){
                    console.log(elementcount);
                    console.log(row);
                    console.log(col);*/
                    break aloop;
                
            }
        }
    }
    aaloop:
    for (var col = 9; col >= 0; col--) {
        for (var row = 0; row <= 19; row++) {
            if (cells[row][col].blockNum == fallingBlockNum ) {
                if (col !== varfirstcol[0] || row !== varfirstrow[0]){
                varfirstcol[elementcount] = col;
                varfirstrow[elementcount] = row;
                elementcount += 1;
                console.log(elementcount);
                if(elementcount == 4){
                    console.log(elementcount);
                    console.log(row);
                    console.log(col);
                    break aaloop;
                    }
                }
            }
        }
    }
    col = varfirstcol[0];
    row = varfirstrow[0];
    var clock = 0;
    /*
    for (var i = 0; i <= 3 ; i++){
        if(cells[row -(col - varfirstcol[i])][col + (row - varfirstrow[i])].blockNum == null){
            clock = 1;
        }
        cells[row -(col - varfirstcol[i])][col + (row - varfirstrow[i])].blockNum = cells[varfirstrow[i]][varfirstcol[i]].blockNum;
        cells[row -(col - varfirstcol[i])][col + (row - varfirstrow[i])].className = cells[varfirstrow[i]][varfirstcol[i]].className;
        if (clock == 1){
        cells[varfirstrow[i]][varfirstcol[i]].blockNum = null;
        cells[varfirstrow[i]][varfirstcol[i]].className = "";
        }
    }
    */
   for (var i = 0; i <= 3 ; i++){
       if(-col + row + varfirstcol[i] > 19 || row + col - varfirstrow[i] > 9 ){
           return;
       }
        if(cells[-col + row + varfirstcol[i]][row + col - varfirstrow[i]].className !== "" && cells[-col + row + varfirstcol[i]][row + col - varfirstrow[i]].blockNum !== fallingBlockNum){
            return;
        }
    }
   for (var i = 0; i <= 3 ; i++){
       clock = 0;
        if(cells[- col + row + varfirstcol[i]][row + col - varfirstrow[i]].blockNum == null){
            clock = 1;
        }
        cells[-col + row + varfirstcol[i]][row + col - varfirstrow[i]].blockNum = cells[varfirstrow[i]][varfirstcol[i]].blockNum;
        cells[-col + row + varfirstcol[i]][row + col - varfirstrow[i]].className = cells[varfirstrow[i]][varfirstcol[i]].className;
        if (clock == 1){
            cells[varfirstrow[i]][varfirstcol[i]].blockNum = null;
            cells[varfirstrow[i]][varfirstcol[i]].className = "";
        }
    }
}